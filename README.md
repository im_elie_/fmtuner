# Flask my tuner

Ce projet Flask a pour but de convertir un Raspberry Pi en un lecteur radio pouvant être contrôlé via une interface web sur le réseau local.

Il utilise le framework Flask pour créer un site web permettant de gérer et contrôler la diffusion radio directement depuis le serveur. Les fonctionnalités comprennent la sélection des stations de radio, le mute/unmute et l'arrêt de la lecture. En exposant cette interface directement depuis le Raspberry Pi, les utilisateurs peuvent accéder à la console du lecteur radio via un navigateur web sur le même réseau local, offrant ainsi une solution de contrôle pratique. Le son est alors diffusé depuis le Raspberry Pi.

## Table des matières

- [Installation](#installation)
- [Utilisation](#utilisation)
- [Configuration](#configuration)
- [Contribuer](#contribuer)
- [Licence](#licence)
- [Credits](#credits)

## Installation

1. Cloner le dépôt :

   ```bash
   git clone https://gitlab.com/im_elie_/fmtuner.git
   ```

2. Aller dans le répertoire du projet :

   ```bash
   cd fmtuner
   ```

3. Créer et activer un environnement virtuel (venv) :

   ```bash
   python -m venv .venv
   source .venv/bin/activate
   ```

4. Installer les dépendances :

   ```bash
   pip install -r requirements.txt
   ```

## Utilisation

1. Lancer l'application Flask :

   ```bash
   python run.py
   ```

   L'application sera accessible à l'adresse [http://127.0.0.1:8080/](http://127.0.0.1:8080/).

2. Accéder à l'application depuis un navigateur.

## Configuration

- **Port et Adresse IP :** Par défaut, l'application écoute sur `0.0.0.0:8080` (cf. [run.py](run.py))

- **Base de Données :** Actuellement, l'application utilise une liste statique pour les radios dans [RADIOS.py](app/RADIOS.py).

## Contribuer

Si vous souhaitez contribuer à ce projet, suivez ces étapes :

1. Forker le projet
2. Créer une branche (`git checkout -b feature/nouvelle-fonctionnalite`)
3. Commiter les changements (`git commit -am 'Ajouter une nouvelle fonctionnalité'`)
4. Pousser la branche (`git push origin feature/nouvelle-fonctionnalite`)
5. Ouvrir une Pull Request

## Licence

Ce projet est sous licence [MIT](LICENSE).

## Credits

Vectors and icons ([mute.svg](app/static/images/mute.svg), [stop.svg](app/static/images/stop.svg) and [unmute.svg](app/static/images/unmute.svg)) by [Shannon E. Thomas](https://dribbble.com/shannonethomas?ref=svgrepo.com) in CC Attribution License via [SVG Repo](https://www.svgrepo.com/).
