# app/routes.py
from app import my_app, RADIOS, MPVPlayer
from flask import render_template, redirect, url_for, abort

@my_app.route('/')
def home():
    return render_template('index.html', radios=RADIOS.RADIOS, playing_radio=MPVPlayer.player.getPlayingRadio())

@my_app.route('/play/<radio_id>')
def play(radio_id):
    try:
        MPVPlayer.player.play(radio_id)
        return redirect(url_for('home'))
    except:
        return abort(500)

@my_app.route('/stop')
def stop():
    try:
        MPVPlayer.player.stop()
        return redirect(url_for('home'))
    except:
        return abort(500)

@my_app.route('/mute')
def mute():
    try:
        MPVPlayer.player.mute()
        return redirect(url_for('home'))
    except:
        return abort(500)

@my_app.route('/unmute')
def unmute():
    try:
        MPVPlayer.player.unmute()
        return redirect(url_for('home'))
    except:
        return abort(500)