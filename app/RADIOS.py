# app/RADIOS.py
RADIOS = [
    {'id': 'RTL', 'name': 'RTL', 'image': 'RTL.webp', 'link': 'http://icecast.rtl.fr/rtl-1-44-128?listen=webCwsBCggNCQgLDQUGBAcGBg'},
    {'id': 'RTL2', 'name': 'RTL2', 'image': 'RTL2.webp', 'link': 'http://icecast.rtl2.fr/rtl2-1-44-128?listen=webCwsBCggNCQgLDQUGBAcGBg'},
    {'id': 'FranceInfo', 'name': 'France Info', 'image': 'FranceInfo.webp', 'link': 'http://icecast.radiofrance.fr/franceinfo-midfi.mp3'},
    {'id': 'FranceInter', 'name': 'France Inter', 'image': 'FranceInter.webp', 'link': 'http://icecast.radiofrance.fr/franceinter-midfi.mp3'},
    {'id': 'FranceCulture', 'name': 'France Culture', 'image': 'FranceCulture.webp', 'link': 'http://icecast.radiofrance.fr/franceculture-midfi.mp3'},
    {'id': 'FranceMusique', 'name': 'France Musique', 'image': 'FranceMusique.webp', 'link': 'http://icecast.radiofrance.fr/francemusique-hifi.aac'},
    {'id': 'Europe1', 'name': 'Europe 1', 'image': 'Europe1.webp', 'link': 'http://stream.europe1.fr/europe1.mp3'},
    {'id': 'Europe2', 'name': 'Europe 2', 'image': 'Europe2.webp', 'link': 'http://europe2.lmn.fm/europe2.mp3'},
    {'id': 'Fip', 'name': 'Fip', 'image': 'Fip.webp', 'link': 'http://icecast.radiofrance.fr/fip-midfi.mp3'},
    {'id': 'Mouv', 'name': 'Mouv\'', 'image': 'Mouv.webp', 'link': 'http://icecast.radiofrance.fr/mouv-midfi.mp3'},
    {'id': 'RadioClassique', 'name': 'Radio Classique', 'image': 'RadioClassique.webp', 'link': 'http://radioclassique.ice.infomaniak.ch/radioclassique-high.mp3'},
    {'id': 'JazzRadio', 'name': 'Jazz Radio', 'image': 'JazzRadio.webp', 'link': 'http://jazzradio.ice.infomaniak.ch/jazzradio-high.mp3'},
    {'id': 'Nostalgie', 'name': 'Nostalgie', 'image': 'Nostalgie.webp', 'link': 'https://scdn.nrjaudio.fm/adwz2/fr/30601/mp3_128.mp3?origine=fluxradios'},
    {'id': 'NRJ', 'name': 'NRJ', 'image': 'NRJ.webp', 'link': 'https://scdn.nrjaudio.fm/adwz2/fr/30001/mp3_128.mp3?origine=fluxradios'},
    {'id': 'RFM', 'name': 'RFM', 'image': 'RFM.webp', 'link': 'http://stream.rfm.fr/rfm.mp3'},
    {'id': 'Skyrock', 'name': 'Skyrock', 'image': 'Skyrock.webp', 'link': 'http://icecast.skyrock.net/s/natio_aac_128k'},
    {'id': 'FunRadio', 'name': 'Fun Radio', 'image': 'FunRadio.webp', 'link': 'http://icecast.funradio.fr/fun-1-44-128?listen=webCwsBCggNCQgLDQUGBAcGBg'},
    {'id': 'CherieFM', 'name': 'Chérie FM', 'image': 'CherieFM.webp', 'link': 'https://scdn.nrjaudio.fm/adwz2/fr/30201/mp3_128.mp3?origine=fluxradios'},
]

def getById(id) :
    for e in RADIOS:
        if e.get('id') == id:
            return e