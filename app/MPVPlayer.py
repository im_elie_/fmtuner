import mpv
from app import RADIOS

class MPVPlayer:
    def __init__(self):
        self.player = mpv.MPV(ytdl=True)
        self.radioId = None

    def play(self, id):
        self.radioId = id
        self.player.play(
            RADIOS.getById(self.radioId).get('link')
        )

    def stop(self):
        self.player.stop()
        self.radioId = None

    def mute(self):
        self.player.mute = True

    def unmute(self):
        self.player.mute = False

    def getPlayingRadio(self):
        return RADIOS.getById(self.radioId)

player = MPVPlayer()
